TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

linux {
EIGENEXT = /home/ydjiang234/Documents/Repo/CPP/eigenextend
STDEXT = /home/ydjiang234/Documents/Repo/CPP/stdextend
LIBPATH = /home/ydjiang234/Documents/CPP
} win32 {
EIGENEXT =C:/Users/0122172s/Documents/Repo/CPP/EigenExtend
STDEXT =C:/Users/0122172s/Documents/Repo/CPP/stdextend
LIBPATH =C:/Users/0122172s/Documents/CPP
}

SOURCES += \
    test/matcfstana.cpp \
    src/Path/generalPath.cpp \
    src/Path/triBackbone.cpp \
    src/Path/unLoadPath1.cpp \
    src/Path/unLoadPath2.cpp \
    src/matCFSTdet.cpp \
    src/matCFSTdetAna.cpp \
    $$EIGENEXT/src/FileIO/FileIO.cpp \
    $$EIGENEXT/src/FileIO/EArrayIO.cpp \
    $$EIGENEXT/src/Optimization/GeneticAlgorithm/chromosome.cpp \
    $$EIGENEXT/src/Optimization/GeneticAlgorithm/gene.cpp \
    $$EIGENEXT/src/Optimization/GeneticAlgorithm/geneint.cpp \
    $$EIGENEXT/src/Optimization/GeneticAlgorithm/generation.cpp \
    src/cfstgeneration.cpp \
    $$STDEXT/src/RandomExtend/randomext.cpp \
    $$STDEXT/src/VectorExtend/vectorext.cpp

HEADERS += \
    src/Path/generalPath.h \
    src/Path/triBackbone.h \
    src/Path/unLoadPath1.h \
    src/Path/unLoadPath2.h \
    src/matCFSTdet.h \
    src/matCFSTdetAna.h \
    $$EIGENEXT/src/FileIO/FileIO.h \
    $$EIGENEXT/src/FileIO/EArrayIO.h \
    $$EIGENEXT/src/Optimization/GeneticAlgorithm/chromosome.h \
    $$EIGENEXT/src/Optimization/GeneticAlgorithm/gene.h \
    $$EIGENEXT/src/Optimization/GeneticAlgorithm/geneint.h \
    $$EIGENEXT/src/Optimization/GeneticAlgorithm/generation.h \
    src/cfstgeneration.h \
    $$STDEXT/src/RandomExtend/randomext.h \
    $$STDEXT/src/VectorExtend/vectorext.h

INCLUDEPATH += \
    src \
    $$EIGENEXT/src \
    $$STDEXT/src \
    $$LIBPATH/eigen-eigen/ \
