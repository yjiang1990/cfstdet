#ifndef CFSTGENERATION_H
#define CFSTGENERATION_H

#include <fstream>
#include <cmath>
#include "Optimization/GeneticAlgorithm/generation.h"
#include "matCFSTdetAna.h"

class CFSTGeneration : public Generation
{
public:
    CFSTGeneration(Eigen::ArrayXXd dataMono, Eigen::ArrayXXd dataCyclic,
                   Eigen::ArrayX2d ranges, std::vector<bool> isInt,
                   size_t population, double selPro=0.6, double mutPro=0.1,
                   size_t crossNum=1);
    double FitnessFun(Chromosome chrom);
    void Optimize1(size_t iterNum);

    Eigen::ArrayXXd dataMono;
    Eigen::ArrayXd dataX, dataY;
    double E, f1, f2, b1, b2;
};

#endif // CFSTGENERATION_H
