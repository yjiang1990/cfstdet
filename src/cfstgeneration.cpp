#include "cfstgeneration.h"

CFSTGeneration::CFSTGeneration(Eigen::ArrayXXd dataMono, Eigen::ArrayXXd dataCyclic,
			   Eigen::ArrayX2d ranges, std::vector<bool> isInt,
			   size_t population, double selPro/*=0.6*/, double mutPro/*=0.1*/,
			   size_t crossNum/*=1*/)
    : Generation(ranges, isInt, population, selPro, mutPro, crossNum)
{
    this->E = dataMono(1,1)/dataMono(1,0);
    this->f1 = dataMono(1,1);
    this->f2 = dataMono(2,1);
    this->b1 = (this->f2 - this->f1) / (dataMono(2,0) - dataMono(1,0)) / this->E;
    this->b2 = (dataMono(3,1) - this->f2) / (dataMono(3,0) - dataMono(2,0)) / this->E;

    this->dataX = dataCyclic.col(0);
    this->dataY = dataCyclic.col(1);
}

double CFSTGeneration::FitnessFun(Chromosome chrom)
{
    Eigen::ArrayXd data = chrom.GetData();

    double revRatio;
    double Dfactor1, Efactor1, Rfactor1;
    double Dfactor2, Efactor2, Rfactor2;
    revRatio = data(0);
    Dfactor1 = data(1);
    Efactor1 = data(2);
    Rfactor1 = data(3);
    Dfactor2 = data(4);
    Efactor2 = data(5);
    Rfactor2 = data(6);

    matCFSTdetana ana(this->E, this->f1, this->f2, this->b1, this->b2, revRatio,
                   Dfactor1, Efactor1, Rfactor1,
                   Dfactor2, Efactor2, Rfactor2);

    Eigen::ArrayXXd out = ana.analysis1(this->dataX);
    double fitness = -1 * std::sqrt((out.col(1) - this->dataY).pow(2).sum());
    return fitness;
}

void CFSTGeneration::Optimize1(size_t iterNum)
{
    this->maxFitness = Eigen::ArrayXd(iterNum);
    this->InitialEvaluate();
    this->maxFitness(this->version-1) = this->fitness.back();
    while (this->version<iterNum) {
        this->Evaluate();
        maxFitness(this->version-1) = this->fitness.back();
        //std::cout << this->chroms.back().GetData().transpose() << std::endl;
        std::cout << this->version << " - ";
        std::cout << this->fitness.back() << std::endl;
    }
    std::ofstream fstr("Para.txt", std::ios::out);
    if (fstr.is_open()) {
       fstr << this->chroms.back().GetData().transpose();
    }
    fstr.close();


    Eigen::ArrayXd data = this->chroms.back().GetData();

    double revRatio;
    double Dfactor1, Efactor1, Rfactor1;
    double Dfactor2, Efactor2, Rfactor2;
    revRatio = data(0);
    Dfactor1 = data(1);
    Efactor1 = data(2);
    Rfactor1 = data(3);
    Dfactor2 = data(4);
    Efactor2 = data(5);
    Rfactor2 = data(6);

    matCFSTdetana ana(this->E, this->f1, this->f2, this->b1, this->b2, revRatio,
                   Dfactor1, Efactor1, Rfactor1,
                   Dfactor2, Efactor2, Rfactor2);

    Eigen::ArrayXXd out = ana.analysis1(this->dataX);

    std::ofstream fstr1("Out1.txt", std::ios::out);
    if (fstr1.is_open()) {
       fstr1 << out;
    }
    fstr1.close();
}
