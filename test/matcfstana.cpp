#include <iostream>
#include <fstream>
#include "matCFSTdetAna.h"
#include "FileIO/EArrayIO.h"
#include "cfstgeneration.h"
using namespace std;

int main(int argc, char *argv[])
{
    if (argc==10) {
        double E, f1, f2, b1, b2, revRatio;
        double Dfactor1, Efactor1, Rfactor1;
        double Dfactor2, Efactor2, Rfactor2;
        string fpMono, fpCyclic;

        revRatio = stod(argv[1]);
        Dfactor1 = stod(argv[2]);
        Efactor1 = stod(argv[3]);
        Rfactor1 = stod(argv[4]);
        Dfactor2 = stod(argv[5]);
        Efactor2 = stod(argv[6]);
        Rfactor2 = stod(argv[7]);
        fpMono = argv[8];
        fpCyclic = argv[9];

        EArrayIO *eaio = new EArrayIO();
        Eigen::ArrayXXd data1 = eaio->loadtxt(fpMono);
        Eigen::ArrayXXd data = eaio->loadtxt(fpCyclic);


        E = data1(1,1)/data1(1,0);
        f1 = data1(1,1);
        f2 = data1(2,1);
        b1 = (f2 - f1) / (data1(2,0) - data1(1,0)) / E;
        b2 = (data1(3,1) - f2) / (data1(3,0) - data1(2,0)) / E;

        matCFSTdetana ana(E, f1, f2, b1, b2, revRatio,
                          Dfactor1, Efactor1, Rfactor1,
                          Dfactor2, Efactor2, Rfactor2);

        ofstream fstr("Out.txt", ios::out);
        if (fstr.is_open()){
            fstr << ana.analysis1(data.col(0));
            fstr.close();
        }

    } else if (argc==5) {
        string fpMono, fpCyclic;
        fpMono = argv[3];
        fpCyclic = argv[4];

        EArrayIO *eaio = new EArrayIO();
        Eigen::ArrayXXd data1 = eaio->loadtxt(fpMono);
        Eigen::ArrayXXd data = eaio->loadtxt(fpCyclic);

        Eigen::ArrayX2d ranges(7,2);
        std::vector<bool> isInt;
        size_t population = stoi(argv[2]);
        double selPro=0.6;
        double mutPro=0.1;
        size_t crossNum=1;

        size_t maxNum = stoi(argv[1]);

        ranges << 0.01, 1.0,
                  0.000001, 0.01,
                  0.000001, 0.01,
                  0.000001, 0.01,
                  0.5, 1.5,
                  0.5, 1.5,
                  0.5, 1.5;

        for (size_t i=0; i<7; ++i) {
            isInt.push_back(false);
        }

        CFSTGeneration gen(data1, data, ranges, isInt, population, selPro, mutPro, crossNum);
        gen.Optimize1(maxNum);
    }
    return 0;
}
