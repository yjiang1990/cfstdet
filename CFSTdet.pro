#-------------------------------------------------
#
# Project created by QtCreator 2018-10-08T12:13:18
#
#-------------------------------------------------

QT       -= core gui

TARGET = CFSTdet
TEMPLATE = lib

DEFINES += CFSTDET_LIBRARY

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

win32: {
DEFINES += WIN_NUIG _USRDLL
OPSPATH = C:/Users/0122172s/Documents/OPS/trunk
} linux {
DEFINES += VB_JYD
OPSPATH = /home/ydjiang234/Documents/OPS/OpenSees
}


SOURCES += \
    src/Path/generalPath.cpp \
    src/Path/triBackbone.cpp \
    src/Path/unLoadPath1.cpp \
    src/Path/unLoadPath2.cpp \
    src/CFSTdet.cpp \
    src/matCFSTdet.cpp \
    src/matCFSTdetAna.cpp \
    $$OPSPATH/DEVELOPER/core/Analysis.cpp \
    $$OPSPATH/DEVELOPER/core/AnalysisModel.cpp \
    $$OPSPATH/DEVELOPER/core/ArrayOfTaggedObjects.cpp \
    $$OPSPATH/DEVELOPER/core/ArrayOfTaggedObjectsIter.cpp \
    $$OPSPATH/DEVELOPER/core/BeamFiberMaterial.cpp \
    $$OPSPATH/DEVELOPER/core/BeamFiberMaterial2d.cpp \
    $$OPSPATH/DEVELOPER/core/BinaryFileStream.cpp \
    $$OPSPATH/DEVELOPER/core/Channel.cpp \
    $$OPSPATH/DEVELOPER/core/ConstraintHandler.cpp \
    $$OPSPATH/DEVELOPER/core/DataFileStream.cpp \
    $$OPSPATH/DEVELOPER/core/DOF_Group.cpp \
    $$OPSPATH/DEVELOPER/core/DOF_GrpIter.cpp \
    $$OPSPATH/DEVELOPER/core/Domain.cpp \
    $$OPSPATH/DEVELOPER/core/DomainComponent.cpp \
    $$OPSPATH/DEVELOPER/core/DummyStream.cpp \
    $$OPSPATH/DEVELOPER/core/EigenSOE.cpp \
    $$OPSPATH/DEVELOPER/core/EigenSolver.cpp \
    $$OPSPATH/DEVELOPER/core/Element.cpp \
    $$OPSPATH/DEVELOPER/core/ElementalLoad.cpp \
    $$OPSPATH/DEVELOPER/core/ElementalLoadIter.cpp \
    $$OPSPATH/DEVELOPER/core/ElementResponse.cpp \
    $$OPSPATH/DEVELOPER/core/FE_Datastore.cpp \
    $$OPSPATH/DEVELOPER/core/FE_EleIter.cpp \
    $$OPSPATH/DEVELOPER/core/FE_Element.cpp \
    $$OPSPATH/DEVELOPER/core/FEM_ObjectBroker.cpp \
    $$OPSPATH/DEVELOPER/core/FiberResponse.cpp \
    $$OPSPATH/DEVELOPER/core/File.cpp \
    $$OPSPATH/DEVELOPER/core/FileIter.cpp \
    $$OPSPATH/DEVELOPER/core/FrictionModel.cpp \
    $$OPSPATH/DEVELOPER/core/FrictionResponse.cpp \
    $$OPSPATH/DEVELOPER/core/Graph.cpp \
    $$OPSPATH/DEVELOPER/core/ID.cpp \
    $$OPSPATH/DEVELOPER/core/IncrementalIntegrator.cpp \
    $$OPSPATH/DEVELOPER/core/Information.cpp \
    $$OPSPATH/DEVELOPER/core/Integrator.cpp \
    $$OPSPATH/DEVELOPER/core/LinearSOE.cpp \
    $$OPSPATH/DEVELOPER/core/LinearSOESolver.cpp \
    $$OPSPATH/DEVELOPER/core/Load.cpp \
    $$OPSPATH/DEVELOPER/core/LoadPatternIter.cpp \
    $$OPSPATH/DEVELOPER/core/MapOfTaggedObjects.cpp \
    $$OPSPATH/DEVELOPER/core/MapOfTaggedObjectsIter.cpp \
    $$OPSPATH/DEVELOPER/core/Material.cpp \
    $$OPSPATH/DEVELOPER/core/MaterialResponse.cpp \
    $$OPSPATH/DEVELOPER/core/Matrix.cpp \
    $$OPSPATH/DEVELOPER/core/Message.cpp \
    $$OPSPATH/DEVELOPER/core/MovableObject.cpp \
    $$OPSPATH/DEVELOPER/core/MP_Constraint.cpp \
    $$OPSPATH/DEVELOPER/core/NDMaterial.cpp \
    $$OPSPATH/DEVELOPER/core/NodalLoad.cpp \
    $$OPSPATH/DEVELOPER/core/NodalLoadIter.cpp \
    $$OPSPATH/DEVELOPER/core/Node.cpp \
    $$OPSPATH/DEVELOPER/core/ObjectBroker.cpp \
    $$OPSPATH/DEVELOPER/core/OPS_Stream.cpp \
    $$OPSPATH/DEVELOPER/core/Parameter.cpp \
    $$OPSPATH/DEVELOPER/core/PlaneStrainMaterial.cpp \
    $$OPSPATH/DEVELOPER/core/PlaneStressMaterial.cpp \
    $$OPSPATH/DEVELOPER/core/PlateFiberMaterial.cpp \
    $$OPSPATH/DEVELOPER/core/Pressure_Constraint.cpp \
    $$OPSPATH/DEVELOPER/core/Recorder.cpp \
    $$OPSPATH/DEVELOPER/core/Renderer.cpp \
    $$OPSPATH/DEVELOPER/core/Response.cpp \
    $$OPSPATH/DEVELOPER/core/SimulationInformation.cpp \
    $$OPSPATH/DEVELOPER/core/SingleDomAllSP_Iter.cpp \
    $$OPSPATH/DEVELOPER/core/SingleDomEleIter.cpp \
    $$OPSPATH/DEVELOPER/core/SingleDomMP_Iter.cpp \
    $$OPSPATH/DEVELOPER/core/SingleDomNodIter.cpp \
    $$OPSPATH/DEVELOPER/core/SingleDomParamIter.cpp \
    $$OPSPATH/DEVELOPER/core/SingleDomPC_Iter.cpp \
    $$OPSPATH/DEVELOPER/core/SingleDomSP_Iter.cpp \
    $$OPSPATH/DEVELOPER/core/SolutionAlgorithm.cpp \
    $$OPSPATH/DEVELOPER/core/SP_Constraint.cpp \
    $$OPSPATH/DEVELOPER/core/StandardStream.cpp \
    $$OPSPATH/DEVELOPER/core/StaticIntegrator.cpp \
    $$OPSPATH/DEVELOPER/core/StringContainer.cpp \
    $$OPSPATH/DEVELOPER/core/Subdomain.cpp \
    $$OPSPATH/DEVELOPER/core/SubdomainNodIter.cpp \
    $$OPSPATH/DEVELOPER/core/TaggedObject.cpp \
    $$OPSPATH/DEVELOPER/core/TransientIntegrator.cpp \
    $$OPSPATH/DEVELOPER/core/UniaxialMaterial.cpp \
    $$OPSPATH/DEVELOPER/core/Vector.cpp \
    $$OPSPATH/DEVELOPER/core/Vertex.cpp \
    $$OPSPATH/DEVELOPER/core/VertexIter.cpp \
    $$OPSPATH/DEVELOPER/core/win32Functions.cpp

HEADERS += \
    src/Path/generalPath.h \
    src/Path/triBackbone.h \
    src/Path/unLoadPath1.h \
    src/Path/unLoadPath2.h \
    src/CFSTdet.h \
    src/matCFSTdet.h \
    src/matCFSTdetAna.h \
    $$OPSPATH/DEVELOPER/core/Analysis.h \
    $$OPSPATH/DEVELOPER/core/AnalysisModel.h \
    $$OPSPATH/DEVELOPER/core/ArrayOfTaggedObjects.h \
    $$OPSPATH/DEVELOPER/core/ArrayOfTaggedObjectsIter.h \
    $$OPSPATH/DEVELOPER/core/BeamFiberMaterial.h \
    $$OPSPATH/DEVELOPER/core/BeamFiberMaterial2d.h \
    $$OPSPATH/DEVELOPER/core/BinaryFileStream.h \
    $$OPSPATH/DEVELOPER/core/Channel.h \
    $$OPSPATH/DEVELOPER/core/classTags.h \
    $$OPSPATH/DEVELOPER/core/ColorMap.h \
    $$OPSPATH/DEVELOPER/core/ConstraintHandler.h \
    $$OPSPATH/DEVELOPER/core/CrdTransf.h \
    $$OPSPATH/DEVELOPER/core/DataFileStream.h \
    $$OPSPATH/DEVELOPER/core/DOF_Group.h \
    $$OPSPATH/DEVELOPER/core/DOF_GrpIter.h \
    $$OPSPATH/DEVELOPER/core/Domain.h \
    $$OPSPATH/DEVELOPER/core/DomainComponent.h \
    $$OPSPATH/DEVELOPER/core/DomainDecompositionAnalysis.h \
    $$OPSPATH/DEVELOPER/core/DummyStream.h \
    $$OPSPATH/DEVELOPER/core/EigenSOE.h \
    $$OPSPATH/DEVELOPER/core/EigenSolver.h \
    $$OPSPATH/DEVELOPER/core/Element.h \
    $$OPSPATH/DEVELOPER/core/ElementalLoad.h \
    $$OPSPATH/DEVELOPER/core/ElementalLoadIter.h \
    $$OPSPATH/DEVELOPER/core/elementAPI.h \
    $$OPSPATH/DEVELOPER/core/ElementIter.h \
    $$OPSPATH/DEVELOPER/core/ElementResponse.h \
    $$OPSPATH/DEVELOPER/core/EquiSolnAlgo.h \
    $$OPSPATH/DEVELOPER/core/FE_Datastore.h \
    $$OPSPATH/DEVELOPER/core/FE_EleIter.h \
    $$OPSPATH/DEVELOPER/core/FE_Element.h \
    $$OPSPATH/DEVELOPER/core/FEM_ObjectBroker.h \
    $$OPSPATH/DEVELOPER/core/Fiber.h \
    $$OPSPATH/DEVELOPER/core/FiberResponse.h \
    $$OPSPATH/DEVELOPER/core/File.h \
    $$OPSPATH/DEVELOPER/core/FileIter.h \
    $$OPSPATH/DEVELOPER/core/FrictionModel.h \
    $$OPSPATH/DEVELOPER/core/FrictionResponse.h \
    $$OPSPATH/DEVELOPER/core/G3Globals.h \
    $$OPSPATH/DEVELOPER/core/Graph.h \
    $$OPSPATH/DEVELOPER/core/ID.h \
    $$OPSPATH/DEVELOPER/core/IncrementalIntegrator.h \
    $$OPSPATH/DEVELOPER/core/Information.h \
    $$OPSPATH/DEVELOPER/core/Integrator.h \
    $$OPSPATH/DEVELOPER/core/LinearSOE.h \
    $$OPSPATH/DEVELOPER/core/LinearSOESolver.h \
    $$OPSPATH/DEVELOPER/core/Load.h \
    $$OPSPATH/DEVELOPER/core/LoadPattern.h \
    $$OPSPATH/DEVELOPER/core/LoadPatternIter.h \
    $$OPSPATH/DEVELOPER/core/MapOfTaggedObjects.h \
    $$OPSPATH/DEVELOPER/core/MapOfTaggedObjectsIter.h \
    $$OPSPATH/DEVELOPER/core/Material.h \
    $$OPSPATH/DEVELOPER/core/MaterialResponse.h \
    $$OPSPATH/DEVELOPER/core/Matrix.h \
    $$OPSPATH/DEVELOPER/core/MeshRegion.h \
    $$OPSPATH/DEVELOPER/core/Message.h \
    $$OPSPATH/DEVELOPER/core/MovableObject.h \
    $$OPSPATH/DEVELOPER/core/MP_Constraint.h \
    $$OPSPATH/DEVELOPER/core/MP_ConstraintIter.h \
    $$OPSPATH/DEVELOPER/core/NDMaterial.h \
    $$OPSPATH/DEVELOPER/core/NodalLoad.h \
    $$OPSPATH/DEVELOPER/core/NodalLoadIter.h \
    $$OPSPATH/DEVELOPER/core/Node.h \
    $$OPSPATH/DEVELOPER/core/NodeIter.h \
    $$OPSPATH/DEVELOPER/core/ObjectBroker.h \
    $$OPSPATH/DEVELOPER/core/OPS_Globals.h \
    $$OPSPATH/DEVELOPER/core/OPS_Stream.h \
    $$OPSPATH/DEVELOPER/core/Parameter.h \
    $$OPSPATH/DEVELOPER/core/ParameterIter.h \
    $$OPSPATH/DEVELOPER/core/PlainMap.h \
    $$OPSPATH/DEVELOPER/core/PlaneStrainMaterial.h \
    $$OPSPATH/DEVELOPER/core/PlaneStressMaterial.h \
    $$OPSPATH/DEVELOPER/core/PlateFiberMaterial.h \
    $$OPSPATH/DEVELOPER/core/Pressure_Constraint.h \
    $$OPSPATH/DEVELOPER/core/Pressure_ConstraintIter.h \
    $$OPSPATH/DEVELOPER/core/Recorder.h \
    $$OPSPATH/DEVELOPER/core/Renderer.h \
    $$OPSPATH/DEVELOPER/core/Response.h \
    $$OPSPATH/DEVELOPER/core/SimulationInformation.h \
    $$OPSPATH/DEVELOPER/core/SingleDomAllSP_Iter.h \
    $$OPSPATH/DEVELOPER/core/SingleDomEleIter.h \
    $$OPSPATH/DEVELOPER/core/SingleDomMP_Iter.h \
    $$OPSPATH/DEVELOPER/core/SingleDomNodIter.h \
    $$OPSPATH/DEVELOPER/core/SingleDomParamIter.h \
    $$OPSPATH/DEVELOPER/core/SingleDomPC_Iter.h \
    $$OPSPATH/DEVELOPER/core/SingleDomSP_Iter.h \
    $$OPSPATH/DEVELOPER/core/SolutionAlgorithm.h \
    $$OPSPATH/DEVELOPER/core/SP_Constraint.h \
    $$OPSPATH/DEVELOPER/core/SP_ConstraintIter.h \
    $$OPSPATH/DEVELOPER/core/StandardStream.h \
    $$OPSPATH/DEVELOPER/core/StaticIntegrator.h \
    $$OPSPATH/DEVELOPER/core/StringContainer.h \
    $$OPSPATH/DEVELOPER/core/Subdomain.h \
    $$OPSPATH/DEVELOPER/core/SubdomainNodIter.h \
    $$OPSPATH/DEVELOPER/core/TaggedObject.h \
    $$OPSPATH/DEVELOPER/core/TaggedObjectIter.h \
    $$OPSPATH/DEVELOPER/core/TaggedObjectStorage.h \
    $$OPSPATH/DEVELOPER/core/TransientIntegrator.h \
    $$OPSPATH/DEVELOPER/core/UniaxialMaterial.h \
    $$OPSPATH/DEVELOPER/core/Vector.h \
    $$OPSPATH/DEVELOPER/core/Vertex.h \
    $$OPSPATH/DEVELOPER/core/VertexIter.h

INCLUDEPATH += \
    src \
    $$OPSPATH/DEVELOPER/core \

unix {
    target.path = /usr/lib
    INSTALLS += target
}
